package bonus;

import com.sun.org.apache.bcel.internal.classfile.JavaClass;
import com.sun.org.apache.bcel.internal.generic.*;
import com.sun.org.apache.xalan.internal.xsltc.compiler.Constants;
import org.apache.bcel.Repository;
import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.generic.*;
import java.io.IOException;
import java.nio.file.Paths;

/**
 * This class tries to inject a method into a .class bytecode
 */
public class ByteManipulator {

    public static void execute(Class cls) {
        String className = cls.getName();
        JavaClass mod = null;
        try {
            mod = org.apache.bcel.Repository.lookupClass(cls);
        } catch (Exception e) {
            System.out.println(e);
        }

        ClassGen modClass = new ClassGen(mod);
        ConstantPoolGen cp = modClass.getConstantPool();
        InstructionList il = new InstructionList();

        il.append(new GETSTATIC(cp.addFieldref("java.lang.System", "out", "Ljava/io/PrintStream;")));
        il.append(new PUSH(cp, "I was inserted here"));
        il.append(new INVOKEVIRTUAL(cp.addMethodref("java.io.PrintStream", "println", "(Ljava/lang/String;)V")));
        il.append(new RETURN());

        MethodGen methodGen = new MethodGen(
                Constants.ACC_PUBLIC,
                Type.VOID,
                new Type[]{new ArrayType(Type.STRING, 1), new ObjectType("java.lang.Integer")},
                new String[]{"args", "value"},
                "INSERTED",
                className,
                il,
                cp);

        methodGen.setMaxLocals();
        methodGen.setMaxStack();
        modClass.addMethod(methodGen.getMethod());
        modClass.update();

        try {
            JavaClass newClass = modClass.getJavaClass();
            String className2 = className.replace(".", "/");
            newClass.dump(className2 + ".class");
            System.out.println("Class " + className + " modified");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
