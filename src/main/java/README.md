# Laboratorul 12

[Link cerinta](https://profs.info.uaic.ro/~acf/java/labs/lab_12.html)

## Compulsory

* The input will be a .class file, located anywhere in the file system.
* Load the specified class in memory, identifying dynamically its package.
* Using reflection, extract as many information about the class (at least its methods).
* Using reflection, invoke the static methods, with no arguments, annotated with @Test.

## Optional

* The input may be a folder (containing .class files) or a .jar. You must explore it recursively.
* Create the complete prototype, in the same manner as javap tool.
* Identify all public classes annotated with @Test and invoke the methods annotated with @Test, whether static or not.  
If a method requires primitive (at least int) or String arguments, generate mock values for them.
* Print a statistics regarding the tests.

## Rezolvare Optional

* Pentru parcurgerea recursiva am creat clasa DirectoryLoader. Pentru foldere se foloseste metoda WalkDirectory si pentru
.jar metoda loadJar 
* Pentru simularea comportamentului "javap" am facut clasa JavaPCopy care primeste un fisier class si afiseaza numele, 
  variabilele si metodele acesteia. 
* Am facut o functie isClassTestAdnot pentru a verifica daca o clasa are adnotarea @Test;
  Pentru apelul claselor cu adnotarea @Test am folosit metodele callStaticTest si CallClass.


## Bonus

* Consider the case when the input files are .java files and compile the source code before analyzing them. (use Java Compiler, for example).
* Using additional annotations, implement non-functional tests over the methods in order to test their reliability and efficiency.
* Use a bytecode manipulation and analysis framework, such as ASM, BCEL or Javassist in order to extract the bytecode of the class, perform bytecode instrumentation (inject code in some method) and generate dynamically a class.


## Rezolvare Bonus

* Am facut clasa Compiler pentru a rula comanda 'javac path/*.java' pentru fisierul .java dat ca parametru pentru metoda compilePath.
* Am folosit BCEL, clasa ByteManipulator incearca sa insereze cod intr-o clasa.