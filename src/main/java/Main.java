import optional.javap.JavaPCopy;
import java.io.File;
import java.nio.file.Paths;

public class Main {
    public static void main(String[] args) {
        File classFile = new File(Paths.get("", "src\\main\\java\\compulsory\\myclass\\Cat.class").toString());
        JavaPCopy jp=new JavaPCopy(classFile);
        jp.execute();
    }
}