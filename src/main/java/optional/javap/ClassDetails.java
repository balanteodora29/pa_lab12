package optional.javap;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

/**
 * This class helps with printing the details about a class:
 * name, package, methods names methods' modifiers.
 */
public class ClassDetails {
    Class<?> Class;

    public ClassDetails(java.lang.Class<?> myClass) {
        Class = myClass;
    }

    /**
     * This method prints the class name, the package name and the names of the methods within this class.
     */
    public void classInfo() {
        System.out.println("Class name is: " + this.Class.getName());
        System.out.println("Class package is: " + this.Class.getPackage());
        try {
            Method[] methods = this.Class.getDeclaredMethods();
            for (Method m : methods) {
                System.out.println("Method name: " + m.getName());
                this.showModifiers(m);
            }
        }catch (NoClassDefFoundError e) {
            System.out.println(e);
        }
    }

    /**
     * This method prints the class method with the method modifiers
     * @param cls
     */
    public static void ShowMethod(Class cls) {
        Method[] methods = cls.getDeclaredMethods();
        for (Method m : methods) {
            ClassDetails.showModifiers(m);
            System.out.print(m.getName() + " ( ");
            Class[] param = m.getParameterTypes();
            for (Class c : param) {
                System.out.print(c.getName() + ", ");
            }
            System.out.println(" ) ");
        }
    }

    /**
     * This method calls all the methods with @Test annotation of the class.
     * For the methods with parameters, for a String parameter calls it with the strig "test"
     * and for the rest, tries with the value 0.
     */
    public static void CallClass(Class cls) {
        Method[] methods = cls.getDeclaredMethods();
        int value = 0;
        int argumentsTotal=0;
        int methodsArguments=0;
        try {
            for (Method m : methods) {
                m.setAccessible(true);
                if (ClassDetails.isMethodTestAdnot(m)) {
                    Class[] param = m.getParameterTypes();
                    Object[] generated = new Object[m.getParameterCount()];
                    if(m.getParameterCount()>0)
                        methodsArguments+=1;
                    int count = 0;
                    for (Class p : param) {
                        if (p.getName().contains("String"))
                            generated[count] = "test";
                        else
                            generated[count] = 0;
                        count++;
                    }
                    argumentsTotal+=count;
                    System.out.println("Calling method \"" + m.getName() + "\"");
                    m.invoke(m.getClass(), generated);
                    value++;
                }
            }
            System.out.println("Finished calling @Test methods");
            System.out.println("Found " + value + " methods");
            System.out.println("Methods with arguments: "+methodsArguments);
            System.out.println("Total generated arguments: "+argumentsTotal);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * This method prints the modifiers of a given method as parameter.
     * @param method
     */
    public static void showModifiers(Method method) {
        int modifiers = method.getModifiers();
        if (Modifier.isPublic(modifiers)) System.out.println("Public method");
        if (Modifier.isPrivate(modifiers)) System.out.println("Private method");
        if (Modifier.isProtected(modifiers)) System.out.println("Protected method");
        if (Modifier.isAbstract(modifiers)) System.out.println("Abstract method");
        if (Modifier.isFinal(modifiers)) System.out.println("Final method");
        if (Modifier.isStatic(modifiers)) System.out.println("Static method");
    }

    /**
     * This method checks if a class has the @Test annotation.
     * @param cls
     * @return true/false depending on the case
     */
    public static boolean isClassTestAdnot(Class cls) {
        Annotation[] annotations = cls.getAnnotations();
        for (Annotation a : annotations) {
            Class<? extends Annotation> type = a.annotationType();
            if (type.getSimpleName().equals("Test"))
                return true;
        }
        return false;
    }

    /**
     * This method calls a class with the @Test annotation.
     * @param cls
     */
    public static void callClassTest(Class cls) {
        if (ClassDetails.isClassTestAdnot(cls)) {
            System.out.println("Class is public and annotated with @Test");
            System.out.println("Starting the method call");
            ClassDetails.CallClass(cls);
        }
    }

    /**
     * This method checks if a method given as parameter has the @Test annotation.
     * @param m (method)
     * @return true/false depending on the case
     */
    public static boolean isMethodTestAdnot(Method m) {
        Annotation[] annotations = m.getAnnotations();
        for (Annotation a : annotations) {
            Class<? extends Annotation> type = a.annotationType();
            if (type.getSimpleName().equals("Test"))
                return true;
        }
        return false;
    }

    /**
     * This method searches through the methods of the class for one with the annotation @Test
     * and calls it if it doesn't have arguments and is static.
     */
    public void callStaticTest() {
        System.out.println("Calling static methods");
        Method[] methods = this.Class.getDeclaredMethods();
        for (Method m : methods) {
            m.setAccessible(true);
            Annotation[] annotations = m.getAnnotations();
            for (Annotation a : annotations) {
                Class<? extends Annotation> type = a.annotationType();
                if (type.getSimpleName().equals("Test")) {
                    if (m.getParameterCount() == 0) {
                        try {
                            m.invoke(this.Class);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        System.out.println("Done with static methods");
    }
}
