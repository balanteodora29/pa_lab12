package optional.javap;

import optional.loaders.PathLoader;

import java.io.File;
import java.lang.reflect.Field;

/**
 * This class tries to mimic the result of the command "javap file.class"
 */
public class JavaPCopy {
    private File classFile;
    private Class cls;

    public JavaPCopy(File classFile) {
        this.classFile = classFile;
        PathLoader loader = new PathLoader(classFile);
        this.cls = loader.load();
    }

    public void execute() {
        System.out.print(cls.getName().substring(cls.getName().indexOf(".") + 1));
        System.out.print(" extends ");
        System.out.print(cls.getSuperclass().getName().substring(cls.getSuperclass().getName().indexOf(".") + 1));
        System.out.println("");
        Field[] fields = cls.getDeclaredFields();
        for (Field f : fields) {
            System.out.print(f.getType().getName()+" ");
            System.out.println(f.getName());
        }
        ClassDetails.ShowMethod(this.cls);
    }
}
