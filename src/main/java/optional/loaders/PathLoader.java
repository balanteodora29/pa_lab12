package optional.loaders;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;

/**
 * This class tries to load a class from a given class by trying every combination of the words in the path
*/
public class PathLoader {
    File classFile;

    public PathLoader(File classFile) {
        this.classFile = classFile;
    }

    public Class<?> load() {
        Class<?> cls = null;

        StringBuilder fileName = null;
        if (classFile.getAbsolutePath().contains(".class"))
            fileName = new StringBuilder(classFile.getName().substring(0, classFile.getName().indexOf(".class")));
        else fileName = new StringBuilder(classFile.getName());
        File extraFile = new File(classFile.getAbsolutePath());
        extraFile = extraFile.getParentFile();
        while (cls == null) {
            try {
                URL url = extraFile.toURI().toURL();
                URL[] urls = new URL[]{url};
                ClassLoader cl = new URLClassLoader(urls);
                cls = cl.loadClass(fileName.toString());
            } catch (NoClassDefFoundError | Exception e) {
                System.out.println("Working on building the class");
            }
            fileName.insert(0, extraFile.getName() + ".");
            extraFile = extraFile.getParentFile();
        }
        return cls;
    }
}
