package optional.loaders;

import optional.javap.ClassDetails;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * This class loads recursively a directory or a .jar file.
 */
public class DirectoryLoader {
    File directory;

    public DirectoryLoader(File classFile) {
        this.directory = classFile;
    }

    /**
     * This method loads the file given as parameter and prints details about it using an ClassDetails object.
     */
    public void load(File f) {
        PathLoader p = new PathLoader(f);
        Class<?> cls = p.load();
        ClassDetails detailer = new ClassDetails(cls);
        detailer.classInfo();
        detailer.callStaticTest();
    }

    public void walkDirectory(File root) {
        File[] list = root.listFiles();
        if (list == null) return;
        for (File f : list) {
            if (f.isDirectory()) {
                walkDirectory(f);
            } else {
                if (f.getAbsolutePath().endsWith(".class"))
                    this.load(f);
            }
        }
    }

    /**
     * This method loads a .jar file and prints information about it.
     */
    public void loadJar(File root) {
        try {
            JarFile jarFile = new JarFile(root.getAbsolutePath());
            Enumeration<JarEntry> e = jarFile.entries();

            URL[] urls = {new URL("jar:file:" + root.getAbsolutePath() + "!/")};
            URLClassLoader cl = URLClassLoader.newInstance(urls);

            while (e.hasMoreElements()) {
                JarEntry je = e.nextElement();
                if (je.isDirectory() || !je.getName().endsWith(".class")) {
                    continue;
                }
                String className = je.getName().substring(0, je.getName().length() - 6);
                className = className.replace('/', '.');
                Class c = cl.loadClass(className);
                ClassDetails detailer = new ClassDetails(c);
                detailer.classInfo();
            }
        } catch (Exception e) {
        }
    }
}
