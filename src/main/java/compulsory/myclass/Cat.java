package compulsory.myclass;

import org.junit.Test;

/**
 * (Used for testing)
 * This class describes a cat.
 */
public class Cat {
    private String breed;
    private String name;

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * (Used for testing)
     * This static method prints a warning message.
     */
    @Test
    private static void warningMessage() { System.out.println("WARNING!!"); }
}