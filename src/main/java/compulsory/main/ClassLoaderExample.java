package compulsory.main;

import compulsory.myclass.ClassDetails;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

/**
 * This class tries to print the info of a class from a given path.
 */
public class ClassLoaderExample {
    public static void main (String[] args) {
        File file = new File("C:\\Users\\teodo\\IdeaProjects2\\PA_Lab12\\src\\main\\java\\");
        try {
            URL url = file.toURI().toURL();
            URL[] urls = new URL[]{url};
            ClassLoader cl = new URLClassLoader(urls);
            Class cls = cl.loadClass("compulsory.myclass.Cat");
            ClassDetails details = new ClassDetails(cls);
            details.classInfo();
            details.callStaticMethods();
        } catch (MalformedURLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
